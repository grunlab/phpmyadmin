# GrunLab phpMyAdmin

phpMyadmin deployment on Kubernetes.

Docs: https://docs.grunlab.net/install/phpMyAdmin.md

GrunLab project(s) using this service:
- [grunlab/vigixplorer][vigixplorer]

[vigixplorer]: <https://gitlab.com/grunlab/vigixplorer>

